from django.conf.urls import url
from django.contrib import admin

import core.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^students/$', core.views.StudentListView.as_view(), name='student-list'),
    url(r'^students/create/$', core.views.StudentCreateView.as_view(), name='student-create'),
    url(r'^students/(?P<pk>[0-9]+)/update/$', core.views.StudentUpdateView.as_view(), name='student-update'),
    url(r'^students/(?P<pk>[0-9]+)/$', core.views.StudentDetailView.as_view(), name='student-detail'),
    url(r'^students/(?P<pk>[0-9]+)/delete/$', core.views.StudentDeleteView.as_view(), name='student-delete'),
    url(r'^payments/create/$', core.views.PaymentCreateView.as_view(), name='payment-create'),
    url(r'^payments/(?P<pk>[0-9]+)/delete/$', core.views.PaymentDeleteView.as_view(), name='payment-delete'),

]
