# Developer Assessment - Student Fees

## Installation

First clone the repository onto your server. Then create a virtual environment that uses python 3.
Once in this new virtual environment you can install the required packages, that includes Django.

```
git clone https://py5gol@bitbucket.org/py5gol/studentfees.git

cd studentfees

python3 -m venv myenv

source ~/myenv/bin/activate

pip install -r requirements.txt
```

This project comes with a sqlite database (db.sqlite3) and a few records. If you wish to start fresh,
delete the database file, db.sqlite3, and run the command below.

```
python manage.py migrate
```

Once that is all done you should be able to run the manage.py script to run the development server.

```
python manage.py runserver
```

Now, you can access the system at http://localhost:8000/students.
