from django.db import models
from django.urls import reverse


class Student(models.Model):

    """
    This class represents students
    """

    surname = models.CharField(

        max_length=64
    )

    forename = models.CharField(

        max_length=64
    )

    def __str__(self):

        return "{} {}".format(self.forename, self.surname)

    def get_absolute_url(self):

        return reverse('student-detail', args=[str(self.id)])


class Payment(models.Model):

    """
    This class represents payments of a specific student
    """

    student = models.ForeignKey(

        Student,
        on_delete=models.CASCADE,
        related_name='payment_list'
    )

    amount = models.DecimalField(

        decimal_places=2,
        max_digits=9
    )

    date = models.DateField(

    )

    def __str__(self):

        return "Student {} {}, date {}, and amount {}".format(self.student.forename, self.student.surname, self.date, self.amount)


    def get_absolute_url(self):

        return reverse('student-detail', args=[str(self.student.id)])
