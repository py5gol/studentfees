from django.contrib import admin

from core.models import Student, Payment

admin.site.register(Student)
admin.site.register(Payment)
