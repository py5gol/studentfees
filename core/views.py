from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView

from core.models import Student, Payment


class StudentListView(ListView):

    model = Student
    ordering = ['surname', 'forename']


class StudentDetailView(DetailView):
    model = Student


class StudentUpdateView(SuccessMessageMixin, UpdateView):
    model = Student
    fields = ['forename', 'surname']
    success_message = "Student record %(forename)s %(surname)s was updated successfully"


class StudentCreateView(SuccessMessageMixin, CreateView):
    model = Student
    fields = ['forename', 'surname']
    success_message = "Student record %(forename)s %(surname)s was created successfully"


class StudentDeleteView(DeleteView):
    model = Student
    success_url = reverse_lazy('student-list')


class PaymentCreateView(SuccessMessageMixin, CreateView):
    model = Payment
    fields = ['date', 'amount']
    success_message = "Payment record %(date)s and %(amount)s was created successfully"

    def form_valid(self, form):
        form_date = form.cleaned_data['date']
        form_amount = form.cleaned_data['amount']

        student_id = self.request.GET.get('student')
        student = Student.objects.get(pk=student_id)

        payment = Payment.objects.create(
            student=student,
            date=form_date,
            amount=form_amount
        )

        return redirect(payment.get_absolute_url())


class PaymentDeleteView(DeleteView):
    model = Payment
    success_url = reverse_lazy('student-list')
